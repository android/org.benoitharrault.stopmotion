image: ghcr.io/cirruslabs/flutter:latest

stages:
  - build-debug
  - build-release
  - release
  - deploy

android:build-debug:
  stage: build-debug
  except:
    - tags
    - master
  script:
    # Flutter local configuration
    - echo flutter.sdk=$FLUTTER_PATH > android/local.properties
    - echo sdk.dir=$ANDROID_SDK_PATH >> android/local.properties
    - echo flutter.buildMode=debug >> android/local.properties
    # Android signing
    - echo "$ANDROID_DEBUG_KEYSTORE_FILE" | base64 -d > android/app/my.keystore
    - echo storeFile=my.keystore > android/key.properties
    - echo storePassword=$ANDROID_DEBUG_KEYSTORE_PASSWORD >> android/key.properties
    - echo keyAlias=$ANDROID_DEBUG_KEY_ALIAS >> android/key.properties
    - echo keyPassword=$ANDROID_DEBUG_KEY_PASSWORD >> android/key.properties
    # build flutter app
    - VERSION_NAME="$(grep '^version:' pubspec.yaml | cut -d' ' -f2 | cut -d'+' -f1)"
    - echo "${VERSION_NAME}"
    - VERSION_CODE="$(grep '^version:' pubspec.yaml | cut -d' ' -f2 | cut -d'+' -f2)"
    - echo "${VERSION_CODE}"
    - flutter packages get
    - flutter clean
    - flutter build apk --debug --split-per-abi
    - flutter build apk --debug
    # prepare artifact
    - BASE_APK_FOLDER="build/app/outputs/flutter-apk"
    - APP_NAME="$(grep 'namespace' android/app/build.gradle | cut -d'"' -f2)"
    - >
      if [ "$(find "${BASE_APK_FOLDER}" -name "*.apk")" != "" ]; then
        for APK in ${BASE_APK_FOLDER}/*.apk; do
          mv -v "${APK}" "$(echo "${APK}" | sed "s|\.apk|_${VERSION_CODE}.apk|" | sed "s|/app-|/${APP_NAME}-|")"
        done
      fi
    - find "${BASE_APK_FOLDER}" -name "*.apk" # where are my apk?
  artifacts:
    paths:
      - build/app/outputs/flutter-apk
    expire_in: 1 week
  interruptible: true

android:build-release:
  stage: build-release
  only:
    - master
  except:
    - tags
  script:
    # Flutter local configuration
    - echo flutter.sdk=$FLUTTER_PATH > android/local.properties
    - echo sdk.dir=$ANDROID_SDK_PATH >> android/local.properties
    - echo flutter.buildMode=release >> android/local.properties
    # Android signing
    - echo "$ANDROID_KEYSTORE_FILE" | base64 -d > android/app/my.keystore
    - echo storeFile=my.keystore > android/key.properties
    - echo storePassword=$ANDROID_KEYSTORE_PASSWORD >> android/key.properties
    - echo keyAlias=$ANDROID_KEY_ALIAS >> android/key.properties
    - echo keyPassword=$ANDROID_KEY_PASSWORD >> android/key.properties
    # build flutter app
    - BASE_APK_FOLDER="build/app/outputs/flutter-apk"
    - echo "${BASE_APK_FOLDER}"
    - mkdir -p "${BASE_APK_FOLDER}"
    - VERSION_NAME="$(grep '^version:' pubspec.yaml | cut -d' ' -f2 | cut -d'+' -f1)"
    - VERSION_CODE="$(grep '^version:' pubspec.yaml | cut -d' ' -f2 | cut -d'+' -f2)"
    - APP_NAME="$(grep 'namespace' android/app/build.gradle | cut -d'"' -f2)"
    - TAG_NAME="Release_${VERSION_NAME}_${VERSION_CODE}"
    - echo "${TAG_NAME}"
    - >
      if [ $(git tag -l "${TAG_NAME}") ]; then
        echo "Tag ${TAG_NAME} already exists. Skipping build release."
      else
        flutter packages get
        flutter clean
        flutter build apk --release --split-per-abi
        flutter build apk --release
      fi
    # prepare artifact
    - >
      if [ "$(find "${BASE_APK_FOLDER}" -name "*.apk")" != "" ]; then
        for APK in ${BASE_APK_FOLDER}/*.apk; do
          mv -v "${APK}" "$(echo "${APK}" | sed "s|\.apk|_${VERSION_CODE}.apk|" | sed "s|/app-|/${APP_NAME}-|")"
        done
      fi
    - find "${BASE_APK_FOLDER}" -name "*.apk" # where are my apk?
  artifacts:
    paths:
      - build/app/outputs/flutter-apk
    expire_in: 1 week
  interruptible: true

application:release:
  stage: release
  image:
    name: alpine/git
    entrypoint: [""]
  only:
    - master
  except:
    - tags
  dependencies:
    - android:build-release
  script:
    - apk --no-cache add curl
    - git config user.email "${GITLAB_USER_EMAIL}"
    - git config user.name "${GITLAB_USER_NAME}"
    - git remote set-url origin https://oauth2:${GITLAB_ACCESS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}
    - VERSION_NAME="$(grep '^version:' pubspec.yaml | cut -d' ' -f2 | cut -d'+' -f1)"
    - VERSION_CODE="$(grep '^version:' pubspec.yaml | cut -d' ' -f2 | cut -d'+' -f2)"
    - TAG_NAME="Release_${VERSION_NAME}_${VERSION_CODE}"
    - echo "${TAG_NAME}"
    - >
      if [ $(git tag -l "${TAG_NAME}") ]; then
        echo "Tag ${TAG_NAME} already exists. Skipping release and tag creation."
      else
        git tag -a "${TAG_NAME}" -m "Release ${VERSION_NAME} (${VERSION_CODE})"
        git push origin "${TAG_NAME}"
        curl --silent -d "{\"token\": \"${JABBER_NOTIFICATION_TOKEN}\", \"message\": \"New tag for ${CI_PROJECT_PATH}: ${TAG_NAME}\"}" -H "Content-Type: application/json" -X POST ${JABBER_NOTIFICATION_URL}
      fi

android:deploy:
  stage: deploy
  only:
    - tags
  dependencies:
    - application:release
  script:
    - curl "${REPOSITORY_UPDATE_WEBHOOK}?token=${REPOSITORY_TOKEN}" --fail
