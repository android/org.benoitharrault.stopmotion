import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:stopmotion/cubit/activity/activity_cubit.dart';
import 'package:stopmotion/ui/widgets/take_picture_widget.dart';

class PageEditor extends StatelessWidget {
  const PageEditor({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        return const Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 8),
            Text('[editor]'),
            TakePictureWidget(),
          ],
        );
      },
    );
  }
}
