import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:stopmotion/utils/picture_storage.dart';

class TakePictureWidget extends StatefulWidget {
  const TakePictureWidget({super.key});

  @override
  TakePictureWidgetState createState() => TakePictureWidgetState();
}

class TakePictureWidgetState extends State<TakePictureWidget> {
  CameraController? controller;
  PictureStorage? storage;

  List<String> previousImages = [];
  List<String> debug = [];

  @override
  void initState() {
    loadCamera();
    storage = PictureStorage();
    super.initState();
  }

  loadCamera() async {
    final List<CameraDescription> cameras = await availableCameras();
    controller = CameraController(
      cameras.first,
      ResolutionPreset.max,
      enableAudio: false,
    );

    controller!.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 400,
            child: controller == null
                ? const Center(child: Text("Loading camera..."))
                : !controller!.value.isInitialized
                    ? const Center(child: CircularProgressIndicator())
                    : CameraPreview(controller!),
          ),
          ElevatedButton.icon(
            label: const Text("Take picture"),
            icon: const Icon(UniconsLine.camera),
            onPressed: () async {
              try {
                if ((controller != null) && (controller!.value.isInitialized)) {
                  final XFile image = await controller!.takePicture();
                  printlog('image.path: ${image.path}');
                  debug.add('image.path: ${image.path}');

                  File savedFile = await storage!.writeCounter(File(image.path));
                  debug.add('image.path: ${image.path}');

                  String imagePath = savedFile.path;
                  printlog('imagePath: $imagePath');
                  debug.add('imagePath: $imagePath');

                  previousImages.add(imagePath);
                  setState(() {});
                }
              } catch (e) {
                debug.add('error: $e');
                setState(() {});

                printlog(e.toString());
              }
            },
          ),
          const Text('debug: '),
          Column(
            children: debug.map((String line) {
              return Text(line);
            }).toList(),
          ),
          previousImages.isEmpty
              ? const Text('no previous images')
              : Column(
                  children: previousImages.map((String imagePath) {
                    return Row(
                      children: [
                        // Image.file(File(imagePath)),
                        Text(imagePath),
                      ],
                    );
                  }).toList(),
                ),
        ],
      ),
    );
  }
}
