import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:stopmotion/cubit/activity/activity_cubit.dart';

import 'package:stopmotion/ui/pages/editor.dart';
import 'package:stopmotion/ui/pages/home.dart';
import 'package:stopmotion/ui/pages/player.dart';

class ApplicationConfig {
  // activity parameter: movie type
  static const String parameterCodeMovieType = 'activity.movieType';
  static const String movieTypeValueDefault = 'default';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexEditor = 1;
  static const int activityPageIndexPlayer = 2;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Stop Motion',
    activitySettings: [
      // movie type
      ApplicationSettingsParameter(
        code: parameterCodeMovieType,
        values: [
          ApplicationSettingsParameterItemValue(
            value: movieTypeValueDefault,
            isDefault: true,
          ),
        ],
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      displayBottomNavBar: true,
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageHome();
              },
            );
          },
        ),
        activityPageIndexEditor: ActivityPageItem(
          code: 'page_editor',
          icon: Icon(UniconsLine.edit),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageEditor();
              },
            );
          },
        ),
        activityPageIndexPlayer: ActivityPageItem(
          code: 'page_player',
          icon: Icon(UniconsLine.play),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PagePlayer();
              },
            );
          },
        ),
      },
    ),
  );
}
