class Picture {
  final String key;
  final String file;

  const Picture({
    required this.key,
    required this.file,
  });

  @override
  String toString() {
    return '$Picture(${toJson()})';
  }

  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'file': file,
    };
  }
}
